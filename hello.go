package hello

//import "fmt"
import "rsc.io/quote"

// Hello print hello message
func Hello(name string) string {
	//message := fmt.Sprintf("Hello %v", name)
	return quote.Hello()
}
