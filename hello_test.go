package hello

import "testing"

func TestHello(t *testing.T) {
    want := "Hello, world."
    if got := Hello("world"); got != want {
        t.Errorf("Hello(world) = %q, want %q", got, want)
    }
}